﻿using UnityEngine;

public class DisablingOutline : MonoBehaviour
{
    private void Start()
    {
        transform.GetComponent<cakeslice.Outline>().enabled = false;
    }
}
