﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : PlayerControllerTest
{
    // Gameobject to control the "trigger volume" using Overlap Box
    [SerializeField] private Transform _triggerPos;
    // Range of X-Axis of the overlap box
    [SerializeField] private float _triggerBoxX;
    // Range of Y-Axis of the overlap box
    [SerializeField] private float _triggerBoxY;
    // Range of Z-Axis of the overlap box
    [SerializeField] private float _triggerBoxZ;

    [SerializeField] private LayerMask _counterLayers;

    [Header("ChefAudios")]
    [SerializeField] private AudioClip[] _chimesAudio;
    [SerializeField] private AudioClip[] _ordersReady;
    [SerializeField] private AudioClip[] _happySounds;
    [SerializeField] private AudioClip[] _unhappySounds;
    [SerializeField] private AudioClip[] _gotFoodSounds;
    [SerializeField] private AudioClip[] _gotIngredientSounds;

    [Header("Particle For Delivery")]
    [SerializeField] private GameObject _particleDeliveryFood;

    private AudioSource audioSource;

    private cakeslice.Outline foodCollidingBefore;
    // 0 - food
    // 1 - counter/delivery/thrash/rice
    private int colliderType;

    private bool _isHoldingFood = false;
    private string _holdingFood = "empty";

    protected override void Awake()
    {
        base.Awake();
        audioSource = GetComponent<AudioSource>();
    }

    protected override void Update() {
        /* string[] names = Input.GetJoystickNames();   
        Debug.Log(names[0]);
        Debug.Log(names[1]);*/
        base.Update();
        if (Input.GetButtonDown("Fire1")) 
        {
            CheckCollision();  
        }

       
        _animator.SetBool("isHolding", _isHoldingFood);
        
        GlowIngredients();
    }

    private void RemovingGlow() 
    {
        if (colliderType == 1) 
        {
            Destroy(foodCollidingBefore);
            colliderType = 0;
        }
        else 
        {
            colliderType = 0;
            foodCollidingBefore.enabled = false;
        }
    }

    private void GlowIngredients() 
    {
        
        Collider[] colliders = Physics.OverlapBox(_triggerPos.position, new Vector3(_triggerBoxX, _triggerBoxY, _triggerBoxZ), Quaternion.identity, _counterLayers);

        if (colliders.Length == 0) 
        {
            if (foodCollidingBefore != null) 
            {
                foodCollidingBefore.enabled = false;
            }
        }

        for (int i = 0; i < colliders.Length; i++) 
        {
            if (colliders[i].CompareTag("KitchenCounterUsed")) 
            {
                Food foodColliding = colliders[i].transform.GetChild(0).GetComponent<Food>();
                if (foodColliding != foodCollidingBefore) 
                {
                    if (foodCollidingBefore != null) 
                    {
                        //cakeslice.Outline outlinerBefore = foodCollidingBefore.GetComponent<cakeslice.Outline>();
                        foodCollidingBefore.enabled = false;
                    }
                }
                cakeslice.Outline outliner = foodColliding.GetComponent<cakeslice.Outline>();
                if (outliner != null) 
                {
                    outliner.enabled = true;
                }
                foodCollidingBefore = outliner;
                break;    
            }

            if (colliders[i].CompareTag("Thrash") || colliders[i].CompareTag("DeliveryArea")) 
            {
                if (foodCollidingBefore != null) 
                {
                    //cakeslice.Outline outlinerBefore = foodCollidingBefore.GetComponent<cakeslice.Outline>();
                    foodCollidingBefore.enabled = false;
                }
                //cakeslice.Outline outliner = colliders[i].transform.gameObject.AddComponent<cakeslice.Outline>();
                cakeslice.Outline outliner = colliders[i].transform.GetComponent<cakeslice.Outline>();
                if (outliner != null) 
                {
                    outliner.enabled = true;
                }
                foodCollidingBefore = outliner;
                break;
            }

            if (colliders[i].CompareTag("RiceTable")) 
            {
                if (foodCollidingBefore != null) 
                {
                    //cakeslice.Outline outlinerBefore = foodCollidingBefore.GetComponent<cakeslice.Outline>();
                    foodCollidingBefore.enabled = false;
                }

                // Outline the Rice Children
                cakeslice.Outline outliner = colliders[i].transform.GetChild(0).GetComponent<cakeslice.Outline>();
                if (outliner != null) 
                {
                    outliner.enabled = true;
                }
                foodCollidingBefore = outliner;
                break;
            }                
        }
    
    }

    // Terrible Function name, change that later
    private bool CheckCollision() 
    {
        // Check if there is a collision in the Trigger Box
        Collider[] colliders = Physics.OverlapBox(_triggerPos.position, new Vector3(_triggerBoxX, _triggerBoxY, _triggerBoxZ), Quaternion.identity, _counterLayers);
        for (int i = 0; i < colliders.Length; i++) 
        {
            // Get the Kitchen Space component of the collision
            KitchenSpace kitchenSpace = colliders[i].GetComponent<KitchenSpace>();

            if (kitchenSpace != null) 
            {
                // Gets the index of the Kitchen Counter (NOT USING THIS)
                int kitchenIndex = kitchenSpace.GetKitchenCounterIndex();
                // Gets food index from the counter (if there is one, or else "empty")
                string foodIndex = Kitchen.kitchenPositions[kitchenIndex];
                
                // Check if the Player is holding food
                if (_isHoldingFood) 
                {
                    // If the counter space is empty - Drop the food on the counter
                    if (colliders[i].CompareTag("KitchenCounterFree")) 
                    {
                        
                        // Gets the child from the Player (the child at the index 0 is the Trigger to find the colision)
                        // the index 1 is the food (if he is holding something)
                        /// FIX THAT GETCHILD(1) !!!!!! 
                        Transform child = transform.GetChild(4);
                        Food foodChild = child.GetComponent<Food>();
                        foodIndex = foodChild.GetFoodIndex();
                        // Gets the position to spawn the food on the counter 
                        Vector3 foodSpawnPosition = kitchenSpace.GeneratesSpawnPosition(kitchenSpace.transform);
                        // Instatiate the food prefab on the counter
                        GameObject instatiatedFood = Instantiate(FoodList.foodList[foodIndex],foodSpawnPosition, FoodList.foodList[foodIndex].transform.rotation);
                        // Add the food as a child of the counter
                        instatiatedFood.transform.parent = colliders[i].transform;
                    
                        // sets the isHoldingFood to false
                        _isHoldingFood = false;
                        // Change the foodIndex of the Kitchen Counter
                        Kitchen.kitchenPositions[kitchenIndex] = foodIndex;
                        // Set the layer of the counter
                        colliders[i].gameObject.tag = "KitchenCounterUsed";
                        Destroy(child.gameObject);
                        // breaks loop to not try another counter

                        Food newFood = instatiatedFood.GetComponent<Food>();
                        if (_gotFoodSounds != null && _gotFoodSounds.Length > 0 && (newFood.GetIsMeal() || newFood.GetFoodIndex() == "rice")) 
                        { 
                            audioSource.clip = _gotFoodSounds[Random.Range(0, _gotFoodSounds.Length)];
                            audioSource.Play();
                        }

                        if (_gotIngredientSounds != null && _gotIngredientSounds.Length > 0 && !newFood.GetIsMeal()) 
                        { 
                            audioSource.clip = _gotIngredientSounds[Random.Range(0, _gotIngredientSounds.Length)];
                            audioSource.Play();
                        }

                        break; 
                    }
                    else 
                    {
                        // try to assemble the food
                        foodIndex = colliders[i].transform.GetChild(0).GetComponent<Food>().GetFoodIndex();

                        // TO DO: Join the 3 next lines to one function in the Kitchen class
                        // Generates the combination between the food of the counter and the food that the player is holding
                        string foodCombinationString = Kitchen.JoinString(foodIndex, _holdingFood);
                        // Creates a List if the combination
                        List<string> foodCombination = Kitchen.BreakFoodIndex(foodCombinationString);
                        // Generates an index for the food - for example "rice|tomato" 
                        // The same logic is applied to all food generated
                        // Separated by | and alphabetic order
                        // This is the fastest way to access a food, because of the complexity of a hashtable (Dictionary on our case)
                        string possibleFoodIndex = Kitchen.GeneratePossibleFoodCode(foodCombination);
                        // Checks if the combination is a valid food (for example rice + rice is not valid, but rice + tomato, is)
                        if (Kitchen.CheckFood(possibleFoodIndex)) 
                        {
                            // Gets the food from the player
                            Transform childPlayer = transform.GetChild(4);
                            Food foodChild = childPlayer.GetComponent<Food>();
                            foodIndex = foodChild.GetFoodIndex();
                            // Detroy food from the player
                            Destroy(childPlayer.gameObject);
                            // Destroy food from the counter
                            foreach(Transform child in colliders[i].transform) 
                            {
                                Destroy(child.gameObject);
                            }

                            // Gets the counter position to spawn the food
                            Vector3 foodSpawnPosition = kitchenSpace.GeneratesSpawnPosition(kitchenSpace.transform);
                            // Instatiate the new food on the counter
                            GameObject instatiatedFood = Instantiate(FoodList.foodList[possibleFoodIndex], foodSpawnPosition, transform.rotation);
                            instatiatedFood.transform.parent = colliders[i].transform;
                            _isHoldingFood = false;
                            Kitchen.kitchenPositions[kitchenIndex] = foodIndex;
                            // INSTATIATE THE NEW FOOD 
                            // Destroy food 
                            // change kitchen index
                            if (_happySounds!=null) 
                            {                        
                                audioSource.clip = _happySounds[Random.Range(0, _happySounds.Length)];
                                audioSource.Play();
                            }
                            break;
                        }
                        else 
                        {
                            //Feedback - CAN'T ASSEMBLE
                            // Play sound
                            if (_unhappySounds!=null) 
                            { 
                                audioSource.clip = _unhappySounds[Random.Range(0, _unhappySounds.Length)];
                                audioSource.Play();
                            }
                        }
                    }
                }
                // If the player is not holding any food
                else 
                {
                    // Checks if the counter is on use
                    if (colliders[i].CompareTag("KitchenCounterUsed")) 
                    {
                        // Gets the foodIndex from the counter
                        foodIndex = colliders[i].transform.GetChild(0).GetComponent<Food>().GetFoodIndex();
                        // Instantiate the food in the trigger position
                        GameObject instatiatedFood = Instantiate(FoodList.foodList[foodIndex],_triggerPos.position, FoodList.foodList[foodIndex].transform.rotation);
                        // add the new to the player as a child
                        instatiatedFood.transform.parent = gameObject.transform;
                        Destroy(instatiatedFood.GetComponent<Rigidbody>());

                        _isHoldingFood = true;
                        _holdingFood = foodIndex;
                        // Change the kitchen to empty
                        Kitchen.kitchenPositions[kitchenIndex] = "empty";
                        // Sets the counter a free one
                        colliders[i].gameObject.tag = "KitchenCounterFree";
                        // Destroy the food from the counter
                        foreach(Transform child in colliders[i].transform) 
                        {
                            Destroy(child.gameObject);
                        }

                        Food newFood = instatiatedFood.GetComponent<Food>();
                        if (_gotFoodSounds != null && _gotFoodSounds.Length > 0 && (newFood.GetIsMeal() || newFood.GetFoodIndex() == "rice")) 
                        { 
                            audioSource.clip = _gotFoodSounds[Random.Range(0, _gotFoodSounds.Length)];
                            audioSource.Play();
                        }

                        else if (_gotIngredientSounds != null && _gotIngredientSounds.Length > 0 && !newFood.GetIsMeal()) 
                        { 
                            audioSource.clip = _gotIngredientSounds[Random.Range(0, _gotIngredientSounds.Length)];
                            audioSource.Play();
                        }

                        break;
                    }
                    else 
                    {
                        
                    }
                }

            }
            // If the counter is a trash, destroy the food
            if (colliders[i].CompareTag("Thrash")) 
            {
                if (_isHoldingFood) 
                {
                    Transform child = transform.GetChild(4);
                    Food foodChild = child.GetComponent<Food>();
                    string foodIndex = foodChild.GetFoodIndex();
                    GameObject instatiatedFood = Instantiate(FoodList.foodList[foodIndex], colliders[i].transform.position, transform.rotation);
                    Destroy(instatiatedFood.GetComponent<BoxCollider>());
                    instatiatedFood.transform.parent = colliders[i].transform;
                    colliders[i].GetComponent<AudioSource>().Play();
                    Destroy(child.gameObject);
                    Destroy(instatiatedFood, 0.5f);
                    _isHoldingFood = false;
                    PlayerProgress.IncreaseThrashPoints();
                    break;
                }
            }
    
            if (colliders[i].CompareTag("RiceTable")) 
            {
                 Debug.Log("Rice");
                if (!_isHoldingFood) 
                {
                    string foodIndex = "rice";
                    GameObject instatiatedFood = Instantiate(FoodList.foodList[foodIndex],_triggerPos.position, transform.rotation);
                    instatiatedFood.transform.parent = gameObject.transform;   
                    Destroy(instatiatedFood.GetComponent<Rigidbody>());
                    _holdingFood = foodIndex;
                    _isHoldingFood = true;
                    Debug.Log("Rice");
                        //Kitchen.kitchenPositions[kitchenIndex] = foodIndex;
                        // Set the layer of the counter
                    if (_gotFoodSounds != null && _gotFoodSounds.Length > 0) 
                    { 
                        audioSource.clip = _gotFoodSounds[Random.Range(0, _gotFoodSounds.Length)];
                        audioSource.Play();
                    }
                    break;
                }
            }

            if (colliders[i].CompareTag("DeliveryArea")) 
            {
                if (_isHoldingFood) 
                {
                    Transform child = transform.GetChild(4);
                    Food foodChild = child.GetComponent<Food>();
                    if (Kitchen.CanDeliveryMeal(foodChild)) 
                    {
                        if (_particleDeliveryFood != null) 
                        {
                            Instantiate(_particleDeliveryFood, child.transform.position + new Vector3(-0.5f,0f,0f), _particleDeliveryFood.gameObject.transform.rotation);
                        }
                        
                        PlayerProgress.IncreasingDeliveryFoodPoints(foodChild.GetFoodIndex(), foodChild.GetMealScore());

                        Destroy(child.gameObject);
                        _isHoldingFood = false;
                        StartCoroutine(PlayOrderReadyAudio(colliders[i]));

                        //colliders[i].GetComponent<AudioSource>().Play();
                    }
                    else 
                    {
                        // TO DO: FEEEDBACK WRONG FOOD
                        if (_unhappySounds!=null) 
                        { 
                            audioSource.clip = _unhappySounds[Random.Range(0, _unhappySounds.Length)];
                            audioSource.Play();
                        }
                    }
                    break;
                }
            }

        }
        return false;
    }

    IEnumerator PlayOrderReadyAudio(Collider collider) 
    {
        audioSource.clip = _chimesAudio[Random.Range(0, _chimesAudio.Length)];
        audioSource.Play();
        yield return new WaitForSeconds(audioSource.clip.length);
        audioSource.clip = _ordersReady[Random.Range(0, _ordersReady.Length)];
        audioSource.Play();
        yield return new WaitForSeconds(audioSource.clip.length);
        collider.GetComponent<AudioSource>().Play();
    }

    // Check if the space is empty
    private bool isEmpty(string foodIndex, bool toCheckEmpty) 
    {
        if (foodIndex == "empty") 
        {
            return toCheckEmpty;
        }
        else {
            return !toCheckEmpty;
        }
    }

    // Debug the Attack Radius
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(_triggerPos.position, new Vector3(_triggerBoxX, _triggerBoxY, _triggerBoxZ));
    }
}
