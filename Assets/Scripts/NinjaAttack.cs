﻿using UnityEngine;

public class NinjaAttack : MonoBehaviour
{
    [SerializeField] private GameObject _particlePrefab;
    // Gameobject to control the "trigger volume" using Overlap Box
    [SerializeField] private Transform _triggerPos;
    // Range of X-Axis of the overlap box
    [SerializeField] private float _triggerBoxX = 0f;
    // Range of Y-Axis of the overlap box
    [SerializeField] private float _triggerBoxY = 0f;
    // Range of Z-Axis of the overlap box
    [SerializeField] private float _triggerBoxZ = 0f;

    [SerializeField] private LayerMask _counterLayers;
    
    [SerializeField] private AudioSource _audioSource;

    [SerializeField] private AudioClip _audioClip;
    
    private void Awake() 
    {
        if (_audioSource != null && _audioClip != null)
        {
            _audioSource.clip = _audioClip;
        }
        
    }
    
    /* private void Update() 
    {    
        if (Input.GetButtonDown("Player1Attack")) 
        {
            Attack();  
        }
        
    }*/
        
    private void PlayAttackSound() 
    {
        print(_audioSource.clip);
        if (_audioSource != null) 
        {
            if (_audioClip != null) 
            {
                _audioSource.clip = _audioClip;
            }
            _audioSource.Play();
        }
    }

    // Function to check if the trigger is collinding with some ingredient, and then send to the Kitchen
    public void Attack() 
    {
        PlayAttackSound();
        Collider[] colliders = Physics.OverlapBox(_triggerPos.position, new Vector3(_triggerBoxX, _triggerBoxY, _triggerBoxZ), Quaternion.identity, _counterLayers);
        for (int i = 0; i < colliders.Length; i++) 
        {
            // if Rotten Food
            RottenFood rottenFood = colliders[i].GetComponent<RottenFood>();
            if (rottenFood != null) 
            {
                
                HorizontalMovement ninjaPlayerMovement = gameObject.GetComponent<HorizontalMovement>();
                ninjaPlayerMovement.PlayBadFoodCollidingSound();
                rottenFood.SlicePoisonFood(ninjaPlayerMovement);
                colliders[i].gameObject.GetComponent<Collider>().enabled = false;
                colliders[i].gameObject.GetComponent<Projectile>().enabled = false;
                Destroy(colliders[i].gameObject, 0.5f);
            }
            else 
            {// Destroy fruit
                HorizontalMovement ninjaPlayerMovement = gameObject.GetComponent<HorizontalMovement>();
                ninjaPlayerMovement.PlayGoodFoodSlices();
                Food foodDestroyed = colliders[i].GetComponent<Food>();
                foodDestroyed.SliceFood();
                Kitchen.AddFoodToKitchenSpace(foodDestroyed.GetFoodIndex());
                NinjaScreenOrders.CheckSlicedIngredient(foodDestroyed.GetFoodIndex());
                Debug.Log("FOOOD DESTR");
                Debug.Log(foodDestroyed.GetFoodIndex());
                colliders[i].gameObject.GetComponent<Collider>().enabled = false;
                colliders[i].gameObject.GetComponent<Renderer>().enabled = false;
                colliders[i].gameObject.GetComponent<Projectile>().enabled = false;
                Destroy(colliders[i].gameObject, 0.5f);
                if (_particlePrefab != null) 
                {
                    Instantiate(_particlePrefab,transform.position, transform.rotation);
                }
            }
        }

    }

    // Debug the Attack Radius
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(_triggerPos.position, new Vector3(_triggerBoxX, _triggerBoxY, _triggerBoxZ));
    }
}
