﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 5f;
    [SerializeField] private float _jumpSpeed = 12f;
    [SerializeField] private float _rotationRate = 360f;

    private float _xInput = 0f;
    private float _yInput = 0f;
    private bool _jumpPressed;
    private Rigidbody _rigidBody;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    protected virtual void Update()
    {
        _xInput = Input.GetAxis("Vertical");
        _yInput = Input.GetAxis("Horizontal");
        Debug.Log("X_Input"+_xInput);
        //Debug.Log("Y_Input"+_yInput);
        
    }

    private void FixedUpdate()
    {
        //Movement(_xInput, _yInput);
        Movement(_xInput);
        Turn(_yInput);
        //if (_xInput < _)
        //Turn(_yInput);
        // checks if the input is lower than 0 and multiply to -1
       
        /*if (_jumpPressed) 
        {
            newVelocity.y = _jumpSpeed;
        }
        else {
            newVelocity.y = _rigidBody.velocity.y;
        }

        // newVelocity.y = _jumpPressed ? _jumpSpeed : _rigitBody.velocity.y

        
        _jumpPressed = false;*/

    }

    private void Movement(float xInput) {
        var newVelocity = new Vector3(xInput , 0f, 0f) * _moveSpeed;
        newVelocity = transform.TransformVector(newVelocity);
        _rigidBody.velocity = newVelocity;
    }

    private void Turn(float input) {
        transform.Rotate(0, input * _rotationRate * Time.deltaTime, 0);
    }
}
