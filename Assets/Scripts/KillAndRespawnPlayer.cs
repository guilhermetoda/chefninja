﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class KillAndRespawnPlayer : MonoBehaviour
{
    [SerializeField] Transform _respawnPosition;
    [SerializeField] GameObject _playerPrefab;

    private Collider _boxCollider;
    
    private void Awake()
    {
        _boxCollider = GetComponent<Collider>();    
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
        {
            Destroy(other.gameObject);
            Instantiate(_playerPrefab, _respawnPosition.transform.position, _playerPrefab.transform.rotation);
        }
    }
}
