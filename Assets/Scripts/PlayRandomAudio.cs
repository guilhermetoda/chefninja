﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomAudio : MonoBehaviour
{
    [SerializeField] private AudioClip[] _availableAudios;

    private float gameTime = 0f;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        gameTime += Time.deltaTime;
        if (!audioSource.isPlaying && Time.timeScale != 0 && gameTime > 30f) 
        {
            int range = _availableAudios.Length;
            int newRange = PlayerProgress.GetLevel() * 3 + 3;
            if (newRange < range) 
            {
                range = newRange;
            }
            int randomNumber = Random.Range(0, 999);
            if (randomNumber <= 1) 
            {
                audioSource.clip = _availableAudios[Random.Range(0,range)];
                audioSource.Play();
            }
        }


    }
    
}
