﻿using UnityEngine;


// TO DO: Raycast to check if there is an obstacle colliding up ^
//Class that implements the Ninja movement on the top screen
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
public class HorizontalMovement : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 4f; //ninja's speed

    [SerializeField] private AudioSource _audioSource; // Source for all Player's Sounds

    [Header("Jump Mechanics")]
    [SerializeField] private float _jumpSpeed = 12f; // Jump Speed
    [SerializeField] private float _gravityScale = 1f; //Gravity scale during jump and movement
    [SerializeField] private float _jumpingTime = 2f; // Max time of jumping (seconds)
    [SerializeField] private AudioClip _jumpSound; // Clip to audio

    [Header("Crouch Mechanics")]
    [SerializeField] private float _crouchProportion = 1.75f; // proportion of the ninja when crouch compared to the standing size
    [SerializeField] private float _headCheckDistance = 3f; // Distance from the character to the ground

    [Header("Ground Check")]
    [SerializeField] private float _groundCheckDistance = 3f; // Distance from the character to the ground
    [SerializeField] private LayerMask _groundMask; // layers that represent the ground

    [Header("Colision and Slicing Sounds")]
    [SerializeField] private AudioClip[] _badFoodCollisionsSounds; // List of Possible sounds when a bad food hits the player
    [SerializeField] private AudioClip[] _slicingGoodFoodSounds; // List of Possible Good Food Sounds

    private float _initialSpeed; // Initial speed, when the game starts 

    private float _slowTimeLeft = 0f;
    private bool _isInSlowMode = false;
    
    private Rigidbody _rigidBody; // Rigidbody of the player
    private Animator _animator; // Animator
    private BoxCollider _boxCollider; // Box Collider 
    private cakeslice.Outline _playerOutliner; // Player Outliner, used for feedback effects;
    private NinjaAttack _ninjaAttack; //Ninja Attack Class

    private float _xInput = 0f; // X-Axis Input 
    private float _yInput = 0f; // Y-Axis Input 
    
    private bool _jumpBlocked = false; // Sets if the player can Jump or not - USED FOR BAD FOOD EFFECTS
    private bool _jumpPressed; // is jump key pressed?
    private bool _isJumping; // is player is jumping? (When the player holds the jump button)
    private bool _isSmall = false; // is the player crouch?
    private bool _isGrounded = false; // is the player grounded?
    private bool _attackBlocked = false; // Can Attack

    private float _jumpCounter = 0f; // Counter for the jumping - When the counter reaches 0, the player will fall

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
        _boxCollider = GetComponent<BoxCollider>();
        _ninjaAttack = GetComponent<NinjaAttack>();

        _initialSpeed = _moveSpeed; // Saving the initial speed
    }
    
    private void Update()
    {
        // Get Axis movement
        // X for movement and Y for crouch
        _xInput = Input.GetAxis("Player1Horizontal");
        _yInput = Input.GetAxis("Player1Vertical");
        _animator.SetFloat("Speed", _xInput);
        //Checks if the player is grounded
        _isGrounded = GroundCheck();

        // if player is grounded and the jump mechanics is not blocked by a poisoned food
        if (_isGrounded && !_jumpBlocked) 
        {
            // if is grounded he can jump
            _jumpCounter = _jumpingTime;
            if (Input.GetButtonDown("Player1Jump")) 
            {
                _jumpPressed = true;
                PlaySound(1); // Play Jumping Sound
                _animator.SetTrigger("Jump");
            }

        }
        // Player tries to jump FEEDBACK
        if (_jumpBlocked && Input.GetButtonDown("Player1Jump")) 
        {
            // FEEDBACK SOUND 
        }

        // if the player is holding the jump button
        if (Input.GetButton("Player1Jump"))
        {
            // then, decrease the counter
            if (_jumpCounter > 0) 
            {
                _jumpCounter -= Time.deltaTime;
            }
            else 
            {
                // else makes the player fall
                _jumpPressed = false;
            }
        }

        if (Input.GetButtonDown("Player1Attack") && !_attackBlocked) 
        {
            _ninjaAttack.Attack();
            _animator.SetTrigger("Attack");

        }

        // If player releases the jump button
        if (Input.GetButtonUp("Player1Jump")) 
        {
            _jumpCounter = 0;
            _jumpPressed = false;
        }
        
        //if (!_isSmall && Input.GetKeyDown(KeyCode.DownArrow))
        // Crouching mechanics
        // if the player isn't crouching and the Y-Axis is < 0, crouch
        if (!_isSmall && _yInput < 0f)
        {
            // TO DO: CHANGE HERE FOR the Animation
            //transform.localScale -= new Vector3(0f, 0.5f, 0f);
            _animator.SetFloat("Vertical", -1);
            _isSmall = true;
            _boxCollider.size = new Vector3(_boxCollider.size.x, _boxCollider.size.y / _crouchProportion, _boxCollider.size.z);
            _boxCollider.center = new Vector3(_boxCollider.center.x, _boxCollider.center.y / _crouchProportion, _boxCollider.center.z);
        }
        
        //if (_isSmall && Input.GetKeyUp(KeyCode.DownArrow))
        // if the player is crouching and released the Y-Axis, stand up
        if (_isSmall && _yInput >= 0f)
        {
            if (CanStandUp()) 
            {
                // TO DO: CHANGE HERE FOR the Animation
                //transform.localScale += new Vector3(0f, 0.5f, 0);
                _animator.SetFloat("Vertical", 0);
                _isSmall = false;
                _boxCollider.size = new Vector3(_boxCollider.size.x, _boxCollider.size.y * _crouchProportion, _boxCollider.size.z);
                _boxCollider.center = new Vector3(_boxCollider.center.x, _boxCollider.center.y * _crouchProportion, _boxCollider.center.z);
            }
        }

        if (_isInSlowMode) 
        {
            if (_slowTimeLeft <= 0f) 
            {
                _slowTimeLeft = 0f;
                //RESET SLOWMODE
                ResetPlayerSpeed();
                UnHighlightPlayer();
                _isInSlowMode = false;
                _jumpBlocked = false;
                _attackBlocked = false;
            }
            else 
            {
                // Still in SlowMode
                _slowTimeLeft -= Time.deltaTime;
            }
        }

    }

    public void PlayBadFoodCollidingSound() 
    {
        if (_audioSource != null) 
        {
            _audioSource.clip = _badFoodCollisionsSounds[Random.Range(0, _badFoodCollisionsSounds.Length)];
            _audioSource.Play();
        }
    }

    public void PlayGoodFoodSlices()
    {
        if (_audioSource != null) 
        {
            _audioSource.clip = _slicingGoodFoodSounds[Random.Range(0, _slicingGoodFoodSounds.Length)];
            _audioSource.Play();
        }
    }

    private void PlaySound(int sound) 
    {
        if (_audioSource != null) 
        {
            if (sound == 1) 
            {
                _audioSource.clip = _jumpSound;
            }

            _audioSource.Play();
        }
    }

    private void FixedUpdate()
    {
        // applying velocity to move horizontally
        var newVelocity = new Vector3(0f , 0f, _xInput).normalized * _moveSpeed;
        newVelocity = transform.TransformVector(newVelocity);

        // applying velocity to the Y Axis to jump
        if (_jumpPressed) 
        {
            newVelocity.y = _jumpSpeed;
        }
        else 
        {
            newVelocity.y = _rigidBody.velocity.y;
        }

        // decrease gravity to create a better jump control
        if (!_isGrounded) 
        {
            newVelocity.y -= _gravityScale; 
        }

        _rigidBody.velocity = newVelocity;
       
        
    }

    // checks if the player can stand up
    private bool CanStandUp()
    {
        // origin of the raycast
        Vector3 origin = transform.position + new Vector3(0f, 0.2f, 0f);
        // perform raycast
        if(Physics.Raycast(origin, Vector3.up, _headCheckDistance, _groundMask))
        {
            // debug raycast hit
            Debug.DrawRay(origin, Vector3.up * _headCheckDistance, Color.green);
            // return success
            //Debug.Log("GROUND");
            return false;
        }

        // raycast failed
        // debug raycast miss
        Debug.DrawRay(origin, Vector3.up * _headCheckDistance, Color.red);
        //Debug.Log("NOT GROUND");
        // return failure
        return true;
    }

    // checks for the ground
    private bool GroundCheck()
    {
        // origin of the raycast
        Vector3 originLeft = transform.position + new Vector3(-0.4f, 0.2f, 0f);
        Vector3 originRight = transform.position + new Vector3(0.4f, 0.2f, 0f);
        // perform raycast
        bool raycastLeft = Physics.Raycast(originLeft, -Vector3.up, _groundCheckDistance, _groundMask);
        bool raycastRight = Physics.Raycast(originRight, -Vector3.up, _groundCheckDistance, _groundMask);
        if(raycastLeft || raycastRight)
        {
            // debug raycast hit
            Debug.DrawRay(originLeft, -Vector3.up * _groundCheckDistance, Color.green);
            Debug.DrawRay(originRight, -Vector3.up * _groundCheckDistance, Color.green);
            // return success
            //Debug.Log("GROUND");
            return true;
        }

        // raycast failed
        // debug raycast miss
        Debug.DrawRay(originLeft, -Vector3.up * _groundCheckDistance, Color.red);
        Debug.DrawRay(originRight, -Vector3.up * _groundCheckDistance, Color.red);
        //Debug.Log("NOT GROUND");
        // return failure
        return false;
    }

    public void SetPlayerSpeed(float newSpeed) 
    {
        _moveSpeed = newSpeed;
    }

    public void ResetPlayerSpeed() 
    {
        SetPlayerSpeed(_initialSpeed);
    }

    public float GetMoveSpeed() 
    {
        return _moveSpeed;
    }

    public void HighlightPlayer() 
    {
        if (_playerOutliner == null) 
        {
            GameObject playerMesh = GetPlayerMesh();
            if (playerMesh != null) 
            {
                SetOulinerFromPlayerMesh(playerMesh);
            }   
        }
        // This is NOT an ELSE because this needs to run in the first time that the _playerOutliner is assigned
        if (_playerOutliner != null) 
        {
            _playerOutliner.enabled = true;
        }
    }

    public void SetSlowEffect(float slowTime, bool blockJump, bool blockAttack) 
    {
        // PLAY FEEDBACK SOUND - Bad Food Effect
        PlayBadFoodCollidingSound();
        float currentSpeed = GetMoveSpeed();
        float newSpeed = currentSpeed / 2;
        SetPlayerSpeed(newSpeed);
        HighlightPlayer();
        _slowTimeLeft = slowTime;
        _isInSlowMode = true;
        _jumpBlocked = blockJump;
        _attackBlocked = blockAttack;
    }

    public void UnHighlightPlayer() 
    {
        if (_playerOutliner != null) 
        {
            _playerOutliner.enabled = false;
        }
    }

    private void SetOulinerFromPlayerMesh(GameObject playerMesh) 
    {
        cakeslice.Outline outline = playerMesh.GetComponent<cakeslice.Outline>();
        _playerOutliner = outline;
    }

    // function that returns the child that has the Player Mesh
    public GameObject GetPlayerMesh() 
    {
        for (int i = 0; i < transform.childCount; i++) 
        {
            Transform child = transform.GetChild(i);
            if (child.CompareTag("PlayerMesh")) 
            {
                return child.gameObject;
            }
        }
        return null;
        
    }
}


