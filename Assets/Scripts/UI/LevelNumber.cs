﻿using UnityEngine;
using UnityEngine.UI;

public class LevelNumber : MonoBehaviour
{
    private Text _text;

    private void Awake()
    {
        _text = GetComponent<Text>();
    }

    private void Update()
    {
        string levelString;
        switch (PlayerProgress.GetLevel())
        {
            case 0:
                levelString = "Level One";
                break;
            case 1:
                levelString = "Level Two";
                break;
            case 2:
                levelString = "Level Three";
                break;
            default:
                levelString = "";
                break;
        }
        _text.text = levelString;
    }
}
