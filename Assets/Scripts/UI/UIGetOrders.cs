﻿using UnityEngine;
using UnityEngine.UI;

public class UIGetOrders : MonoBehaviour
{
    [SerializeField] private static int _maxOrders = 6;
    private static bool[] _canvasPositionsUsed;

    private void Awake()
    {
        _canvasPositionsUsed = new bool[_maxOrders];
    }

    private static GameObject CreateNewCanvasObject() 
    {
        GameObject canvasObject = new GameObject();
        canvasObject.AddComponent<CanvasRenderer>();
        canvasObject.AddComponent<RectTransform>();
        return canvasObject;
    }

    private static int GetFirstFreePosition() 
    {
        for (int i = 0; i < _canvasPositionsUsed.Length; i++) 
        {
            if (_canvasPositionsUsed[i] == false) 
            {
                return i;
            }
        }
        return _maxOrders;
    }

    public static int SetNewImageToCanvas(Sprite newImage) 
    {
        //GameObject newCanvasObject = CreateNewCanvasObject();
        //Image canvasImage = newCanvasObject.AddComponent<Image>();
        int firstFreePosition = GetFirstFreePosition();
        if (firstFreePosition < _maxOrders) 
        {
            
            Image canvasImage = GameObject.Find("NinjaOrderImage"+firstFreePosition).GetComponent<Image>();
            canvasImage.sprite = newImage;
            canvasImage.enabled = true;
            _canvasPositionsUsed[firstFreePosition] = true;
            return firstFreePosition;
        }
        return _maxOrders;
        //Debug.Log("OrderImage"+firstFreePosition);
        //newCanvasObject.transform.position = new Vector3(0,0,0);
        //GameObject canvas = GameObject.Find("Canvas");
        //newCanvasObject.transform.SetParent(canvas.transform);  
    }

    public static void SetImageToKitchenScreen(Sprite newImage, int position) 
    {
        Image canvasImage = GameObject.Find("OrderImage"+position).GetComponent<Image>();
        canvasImage.sprite = newImage;
        canvasImage.enabled = true;
        _canvasPositionsUsed[position] = true;
    }

    // TO DO: Fix that - it isn't the best way to do it
    public static void RemovingOrderFromCanvas(int canvasPosition) 
    {
        Image canvasImage = GameObject.Find("OrderImage"+canvasPosition).GetComponent<Image>();
        canvasImage.enabled = false;
        _canvasPositionsUsed[canvasPosition] = false;

    }

    public static void RemovingOrderFromNinjaScreen(int canvasPosition) 
    {
        Image canvasImage = GameObject.Find("NinjaOrderImage"+canvasPosition).GetComponent<Image>();
        canvasImage.enabled = false;
    }
}
