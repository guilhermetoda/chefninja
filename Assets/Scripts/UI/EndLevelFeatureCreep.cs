﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndLevelFeatureCreep : MonoBehaviour
{
    
    [SerializeField] private GameObject _nextLevelButton;
    [SerializeField] private GameObject _mainMenuButton;
    [SerializeField] private float _secondsToShow = 1f;
    private bool _canBeEnable =false;
    private float _counter =0f;
    private float _deltaTime;

    float previousTimeSinceStartup;

    private void Awake()
    {
        previousTimeSinceStartup = Time.realtimeSinceStartup;
    }

    private void Update()
    {
        float realtimeSinceStartup = Time.realtimeSinceStartup;
		_deltaTime = realtimeSinceStartup - previousTimeSinceStartup;
		previousTimeSinceStartup = realtimeSinceStartup;

        if (_canBeEnable)
        {
            _counter +=_deltaTime;
            if (_counter >= _secondsToShow) 
            {
                _nextLevelButton.SetActive(true);
                _mainMenuButton.SetActive(true);
                PauseGame.blockInput = false;
                _canBeEnable = false;
            }
        }
    }
    
    private void OnEnable()
    {
        _canBeEnable = true;
    }

}
