﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelTutorial : MonoBehaviour
{
    [SerializeField] private GameObject _UIPanelTutorial;

    private bool _playerHasStarted = false;
    private bool _waitingForInput = false;
    
    private void Awake()
    {
        if (PlayerProgress.GetLevel() == 0 && !PlayerProgress.tutorial) 
        {
            PauseGame.PausingTheGame();
            _UIPanelTutorial.SetActive(true);
            _waitingForInput = true;
        }
        else 
        {
            _UIPanelTutorial.SetActive(false);
            PauseGame.UnPauseTheGame();
        }
    }

    private void Update()
    {
        if (_waitingForInput)
        {
            if (Input.GetButtonDown("StartGame")) 
            {
                _UIPanelTutorial.SetActive(false);
                PauseGame.UnPauseTheGame();
                _waitingForInput = false;
                PlayerProgress.tutorial = true;
            }
        }
    }
}
