﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class BeginLevelCountdown : MonoBehaviour
{
    [SerializeField] private Text[] _counter;

    [Header("Audios")]
    [SerializeField] private AudioSource _audioBegin;
    [SerializeField] private AudioClip _audioThree;
    [SerializeField] private AudioClip _audioTwo;
    [SerializeField] private AudioClip _audioOne;
    [SerializeField] private AudioClip _audioGo;

    [SerializeField] private UnityEvent _beginLevelEvents;

    private bool[] isPlayingArray;

    private AudioSource audioSource;

    private float counter = 5f;

    private void Awake()
    {
        
        audioSource = GetComponent<AudioSource>();
        isPlayingArray = new bool[6];
        isPlayingArray[0] = false;
        isPlayingArray[1] = false;
        isPlayingArray[2] = false;
        isPlayingArray[3] = false;
        isPlayingArray[4] = false;
        isPlayingArray[5] = false;
    }

    private void Update()
    {
        if (counter >= 0f) 
        {
            int intCounter = Mathf.RoundToInt(counter);
            if (!isPlayingArray[intCounter]) 
            {
                PlayAudio(intCounter);
                isPlayingArray[intCounter] = true;
            }

            if (counter > 2f && counter < 4f) 
            {
                _counter[3].enabled = false;
            }
            else if (counter >= 4f) 
            {

            }
            else 
            {            
                _counter[intCounter+1].enabled = false;
            }
            
            if (intCounter < 4) 
            {
                _counter[intCounter].enabled = true;
            }
            
            counter -= Time.deltaTime;   
        }
        else 
        {
            if (_counter[0].enabled) 
            {
                _counter[0].enabled = false;
                if (_beginLevelEvents != null) 
                {
                    _beginLevelEvents.Invoke();
                }
            }
        }
    }

    private void PlayAudio(int count) 
    {
        switch (count)
        {
            case 5:
                break;
            case 4: 
                _audioBegin.Play();
                break;
            case 3: 
                audioSource.clip = _audioThree;
                audioSource.Play();
                break;
            case 2: 
                audioSource.clip = _audioTwo;
                audioSource.Play();
                break;
            case 1: 
                audioSource.clip = _audioOne;
                audioSource.Play();
                break;
            default: 
                audioSource.clip = _audioGo;
                audioSource.Play();
                break;    
        }
            
    }
}
