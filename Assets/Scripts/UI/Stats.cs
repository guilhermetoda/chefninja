﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stats : MonoBehaviour
{
    [SerializeField] private Image[] _imagesList;
    [SerializeField] private Text[] _textList;

    private void ShowStats() 
    {
        int index = 0;
        foreach(var item in PlayerProgress.GetFoodPoints())
        {       
            if (index < _imagesList.Length)
            {    
                //Debug.Log(item.Key+" : "+item.Value);
                if (FoodList.foodList.ContainsKey(item.Key)) 
                {
                    Food food = FoodList.foodList[item.Key].GetComponent<Food>();

                    int numberOfFoodDelivered = item.Value/food.GetMealScore();
                    _imagesList[index].sprite = food.GetStatsPicture();    
                    _textList[index].text = numberOfFoodDelivered.ToString();
                    _imagesList[index].gameObject.SetActive(true);
                    index++;
                }
            }
        }
    }

    private void OnEnable()
    {
        ShowStats();
    }
}
