﻿using UnityEngine;
using UnityEngine.UI;

public class RequirementCoins : MonoBehaviour
{
    private Text _text;

    private void Awake()
    {
        _text = GetComponent<Text>();

    }
    private void Update()
    {
        _text.text = "/"+PlayerProgress.GetCoinsRequired().ToString();
    }
}
