﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CreditsEndFade : MonoBehaviour
{
    [SerializeField] private Image _fadeInOutImage;
    [SerializeField] private Animator _animator;
    [SerializeField] private GameObject _endCreditsUI;

    private void OnEnable()
    {
        Debug.Log("AAA");
        StartCoroutine(FadingOut());
    }

    IEnumerator FadingOut() 
    {
        _animator.SetBool("Fade", true);
        yield return new WaitUntil(()=>_fadeInOutImage.color.a==1);
        _endCreditsUI.SetActive(true);
        yield return new WaitForSeconds(6f);
        SceneManager.LoadScene("Main");
    }
}
