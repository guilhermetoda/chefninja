﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlaySoundWinOrLose : MonoBehaviour
{
    [SerializeField] private AudioClip _youWin;
    [SerializeField] private AudioClip _youLose;

    IEnumerator PlayWinOrLose() 
    {
        AudioSource audioSource = transform.gameObject.AddComponent<AudioSource>();
        if (PlayerProgress.GetIfPlayerPassedLevel()) 
        {
            // Play Win
            audioSource.clip = _youWin;
        }
        else 
        {
            //Play Lose
            audioSource.clip = _youLose;
        }
        yield return new WaitForSeconds(2f);
        audioSource.Play();
    }
}
