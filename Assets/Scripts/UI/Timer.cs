﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{

    [SerializeField] private UnityEvent gameOverEvent;
    [SerializeField] private GameObject EndScreen;
    [SerializeField] private float[] _levelTime;

    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _firstClip;
    [SerializeField] private AudioClip _timesUp;
    [SerializeField] private AudioClip _youWin;
    [SerializeField] private AudioClip _youLose;

    [SerializeField] private AudioClip _firstBeep;
    [SerializeField] private AudioClip _doubleBeep;

    [SerializeField] private GameObject _prefabMinus10;

    public Text timerText; // used for showing countdown

    private float timeLeft = 5f;
    private bool isTiming = true;

    private float _startBeep = 10f;
    private bool[] _beepingArray;

    private void Start() 
    {
        // + 3f for the counter in the beginning of the game
        timeLeft = _levelTime[PlayerProgress.GetLevel()] + 4f;
        timerText.color = Color.black;

        InitializeBeepArray();

    }

    private void InitializeBeepArray() 
    {
        _beepingArray = new bool[(int)_startBeep];
        for (int i=0; i< (int)_startBeep; i++) 
        {
            _beepingArray[i] = false;
        }
    }

    private void Update()
    {
        if (isTiming) 
        {
            timeLeft -= Time.deltaTime;
            
            if (timeLeft > _levelTime[PlayerProgress.GetLevel()]) 
            {
                timerText.text = ":"+(_levelTime[PlayerProgress.GetLevel()]).ToString("0");
            }
            else 
            {
                timerText.text = ":"+(timeLeft).ToString("0");
            }



            if (timeLeft <= _startBeep / 2) 
            {
                timerText.color = Color.red;
                if (!_beepingArray[(int)timeLeft]) 
                {
                    PlayTimerDoubleBeep();
                    _beepingArray[(int)timeLeft] = true;
                }
            } 
            else if (timeLeft <= _startBeep) 
            {
                timerText.color = Color.red;
                if (!_beepingArray[(int)timeLeft]) 
                {
                    PlayTimerSoundFirstBeep();
                    _beepingArray[(int)timeLeft] = true;
                }
            } 

            if (timeLeft <= 0)
            {
                isTiming = false;
                PlayerProgress.DebugDictionary();
                StartCoroutine(EndLevelAudioRoutine());
            }
        }
    }

    private void PlayTimerSoundFirstBeep() 
    {
        _audioSource.clip = _firstBeep;
        _audioSource.Play();
    }

    private void PlayTimerDoubleBeep() 
    {
        _audioSource.clip = _doubleBeep;
        _audioSource.Play();
    }

    private void FeedbackWasteFood() 
    {
        GameObject[] kitchenUsedSpaces = GameObject.FindGameObjectsWithTag("KitchenCounterUsed");
        for (int i=0; i<kitchenUsedSpaces.Length; i++) 
        {
            if (_prefabMinus10!=null) 
            {
                GameObject minus10 = Instantiate(_prefabMinus10, kitchenUsedSpaces[i].transform.position + new Vector3(0.46f, 1.5f, 0f), _prefabMinus10.transform.rotation);
                Destroy(minus10, 1f);
            }
        }
        
    }

    IEnumerator EndLevelAudioRoutine()
    {
        PlayerProgress.EndLevel();
        FeedbackWasteFood();
        if (gameOverEvent != null) 
        {
            gameOverEvent.Invoke();
        }

        EndScreen.SetActive(true);
        // Play Finish Audio
        /*Debug.Log("Coroutine");
        _audioSource.clip = _firstClip;
        _audioSource.Play();*/
        PlayWinOrLose();
        yield return new WaitForSeconds(2f);
        
        Debug.Log("Coroutine 2");
        /* _audioSource.clip = _timesUp;
        _audioSource.Play();
        yield return new WaitForSeconds(0.5f);*/
        
        
        
    }

    private void PlayWinOrLose() 
    {
        AudioSource audioSource = transform.gameObject.AddComponent<AudioSource>();
        if (PlayerProgress.GetIfPlayerPassedLevel()) 
        {
            // Play Win
            audioSource.clip = _youWin;
        }
        else 
        {
            //Play Lose
            audioSource.clip = _youLose;
        }
        audioSource.Play();
    }

    
    

}
