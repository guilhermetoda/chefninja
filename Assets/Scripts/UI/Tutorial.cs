﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _audioMusic;
    [SerializeField] private Text _text;
    [SerializeField] private Image _image;
    
    private bool _setText = false;
    private float _timeToPlay = 4f;
    private float _counterTimer;

    private void Awake()
    {
        _counterTimer = 0f;
    }

    private void Update()
    {
        _counterTimer += Time.deltaTime;

        if (_counterTimer >= _timeToPlay && !_setText) 
        {
            _setText = true;
            StartCoroutine(Blink());
        }

        if (_setText && Input.GetButtonDown("StartGame"))
        {
            StopCoroutine(Blink());
            SceneManager.LoadScene("SplitScreenGame");
            
        }
        
    }

    private IEnumerator Blink() 
    {
        while (true) 
        {
            _text.text = "to start";
            _image.gameObject.SetActive(true); 
            yield return new WaitForSeconds(.5f);
            _text.text = "";
            _image.gameObject.SetActive(false); 
            yield return new WaitForSeconds(.5f);
        }
    }
}


