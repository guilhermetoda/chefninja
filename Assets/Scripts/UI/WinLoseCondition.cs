﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WinLoseCondition : MonoBehaviour
{
    private Text _winOrLoseText;
    [SerializeField] private Text _buttonText;
    [SerializeField] private GameObject _amazingEndButton;

    private void Awake()
    {
        _winOrLoseText = GetComponent<Text>();
    }

    private void Update()
    {
        if (PlayerProgress.GetIfPlayerPassedLevel()) 
        {
            if (PlayerProgress.IsGameFinished()) 
            {
                _winOrLoseText.text = "Thank you for playing!";
                _buttonText.gameObject.transform.parent.gameObject.SetActive(false);
                _amazingEndButton.SetActive(true);
                EventSystem.current.SetSelectedGameObject(_amazingEndButton);
            }
            else 
            {
                _winOrLoseText.text = "You Win!";
                _buttonText.text = "Next Level";
            }
            
        }
        else 
        {
            _winOrLoseText.text = "You Lose";
            _buttonText.text = "Restart Level";
        }
    }
}
