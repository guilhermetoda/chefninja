﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class CreditsEnd : MonoBehaviour
{
    [SerializeField] private GameObject _endCreditsUI;

    private void OnBecameVisible()
    {
        _endCreditsUI.SetActive(true);
    }

}
