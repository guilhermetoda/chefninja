﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class to moves the player in the Kitchen Screen
// This code was partially got from internet
public class PlayerControllerTest : MonoBehaviour 
{
    [SerializeField] float moveSpeed = 4f; //Change in inspector to adjust move speed

    protected Animator _animator;

    Vector3 forward, right; // Keeps track of our relative forward and right vectors

    private float _xInput = 0f;
    private float _yInput = 0f;
    
    protected virtual void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    void Start()
    {
        forward = Camera.main.transform.forward; // Set forward to equal the camera's forward vector
        forward.y = 0; // Set the y position to 0 
        forward = Vector3.Normalize(forward); // normalize the vector
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward; // set the right-facing vector to be facing right relative to the camera's forward vector
    }
    protected virtual void Update()
    {
        _xInput = Input.GetAxis("Vertical");
        _yInput = Input.GetAxis("Horizontal");
        if (_xInput != 0 || _yInput !=0) 
        {
            Move(_xInput, _yInput);
        }
        _animator.SetFloat("Speed", _xInput + _yInput);
        if (_xInput == 0 && _yInput == 0) 
        {
            _animator.SetBool("Idle", true);
        }
        else 
        {
            _animator.SetBool("Idle", false);
        }
    }
    void Move(float x, float y)
    {
        
        Vector3 direction = new Vector3(x, 0, y); // Gets the input and add to the vector
        Vector3 rightMovement = right * moveSpeed * Time.deltaTime * y; // apply the movment to the right
        Vector3 upMovement = forward * moveSpeed * Time.deltaTime * x; // apply movement to the X-axis (of the camera)
        Vector3 heading = Vector3.Normalize(rightMovement + upMovement); // normalize the new direction
        transform.forward = heading; // The new direction is now the combination of foward and right (of the camera)
        transform.position += rightMovement; // move the character to the right (if necessary)
        transform.position += upMovement; //  move the character in the z-axis (if necessary)
        transform.Rotate(heading); // rotate to the new direction
    }
}
    