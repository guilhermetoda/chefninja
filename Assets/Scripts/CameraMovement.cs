﻿using UnityEngine;

//Moves the Ninja camera horizontally
public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float _movementSpeed = 0.5f;
    
    private void Update()
    {
        Vector3 newPosition = new Vector3(transform.position.x - Time.deltaTime * _movementSpeed, transform.position.y, transform.position.z);
        transform.position = newPosition;
    }
}
