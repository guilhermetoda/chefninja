﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMainMenuMusic : MonoBehaviour
{

    private AudioSource audioSource;
    [SerializeField] private AudioClip _firstClip;
    [SerializeField] private AudioClip _loopSong;
    
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(PlayAudio());
    }

    IEnumerator PlayAudio()
    {
        // Play Finish Audio
        Debug.Log("Coroutine");
        yield return new WaitForSeconds(2.5f);
        audioSource.clip = _firstClip;
        audioSource.Play();
       
        
    }
}
