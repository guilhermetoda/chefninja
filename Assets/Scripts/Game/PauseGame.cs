﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    [SerializeField] private GameObject _pauseScreen;
    public static bool blockInput = false;

    private void Update()
    {
        if (Input.GetButtonDown("Pause")) 
        {
            if(Time.timeScale == 1)
			{
				Time.timeScale = 0;
				_pauseScreen.SetActive(true);
			} 
            else if (Time.timeScale == 0)
            {
				Time.timeScale = 1;
				_pauseScreen.SetActive(false);
			}
        }
    }

    public void GameOver() 
    {
        //StartCoroutine(GameOverCourotine());
        blockInput = true;
        PlayerProgress.DebugDictionary();
        PausingTheGame();
    }

    IEnumerator GameOverCourotine()
    {
        yield return new WaitForSeconds(3f);
        blockInput = false;
        PausingTheGame();
    }

    public static void UnPauseTheGame() 
    {
        if(Time.timeScale == 0)
		{
			Time.timeScale = 1;
		}
    }

    public static void PausingTheGame() 
    {
        if(Time.timeScale == 1)
		{
			Time.timeScale = 0;
		}
    }

}
