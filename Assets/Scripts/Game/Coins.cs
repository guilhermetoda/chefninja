﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class to control the coins during the level
public class Coins : MonoBehaviour
{
    private static int _coinsEarned = 0;

    private void Awake()
    {
        _coinsEarned = 0;
    }
    
    // Increment the number of coins
    public static void IncrementCoins(int coins) 
    {
        _coinsEarned += coins;
        Debug.Log("NEW COINS AMOUNT: "+_coinsEarned);
    }

    public static void SetCoins(int newCoinsAmount)
    {
        _coinsEarned = newCoinsAmount;
    }

    public static int GetCoins() 
    {
        return _coinsEarned;
    }

}
