using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Class Created to control the Food
// _foodIndex a string to represent the key of the food in the Hashtable (Dictionary) that has all the foods  
// name of the food to be shown in the game
public class Food : MonoBehaviour
{
    [SerializeField] private string _foodIndex;
    [SerializeField] private string _name;

    private bool allSliced = false;
    private List<Ingredient> ingredientsList;

    [Header("Only for Ingredients")]
    [SerializeField] protected GameObject _prefabSliced;
    [SerializeField] private int _slicedParts = 2; // Number of pieces that the food can be sliced
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private Sprite _wholePicture; // picture of the ingredient not sliced (UI)
    [SerializeField] private Sprite _slicedPicture; // picture of the sliced ingredient (UI)
    
    [Header("Level - Ingredient / Food")]
    [SerializeField] private int _mealFirstLevel = 0;

    [Header("Only for Meals!")]
    [SerializeField] private bool _isMeal;
    [SerializeField] private int _mealScore = 0;
    [SerializeField] private Sprite _mealPicture;
    [SerializeField] private Sprite _ninjaPicture;
    [SerializeField] private Sprite _statsPicture;
    

    private string GetFoodName() 
    {
        return _name;
    }

    public bool GetAllSliced() 
    {
        return allSliced;
    }

    public int GetMealScore() 
    {
        return _mealScore;
    }

    public bool GetIsMeal() 
    {
        return _isMeal;
    }

    public string GetFoodIndex() 
    {
        return _foodIndex;
    }

    public Sprite GetSprite() 
    {
        return _mealPicture;
    }

    public Sprite GetStatsPicture() 
    {
        return _statsPicture;
    }

    public Sprite GetSpriteNinjaScene() 
    {
        return _ninjaPicture;
    }

    private void SetFoodName(string newName) 
    {
        _name = newName;
    }

    public int GetMealFirstLevel() 
    {
        return _mealFirstLevel;
    }

    public bool IsValidFoodForLevel() 
    {
        if (GetMealFirstLevel() <= PlayerProgress.GetLevel()) 
        {
            //Debug.Log("FOOD: "+_foodIndex+" MEAL"+GetMealFirstLevel()+ " - HERE VALID FOOD PL: "+  PlayerProgress.GetLevel());
            return true;
        }
        return false;
        
    }

    public virtual void OnCollisionEffect(Collision other) 
    {
        // Function to create an effect when a food hits something...
    } 

    public bool HasIngredient(string ingredientIndex) 
    {   
        foreach (Ingredient ingredient in ingredientsList)
        {
            if (ingredient.GetName() == ingredientIndex) 
            {
                return true;
            }
        }
        return false;
    }

    public bool SliceIngredient(string ingredientIndex) 
    {   
        foreach (Ingredient ingredient in ingredientsList)
        {
            if (ingredient.GetName() == ingredientIndex && !ingredient.GetIsSliced()) 
            {
                ingredient.SetSliced();
                allSliced = CheckIfAllSlicedIngredients();
                return true;
            }
        }
        return false;
    }

    public bool CheckIfAllSlicedIngredients() 
    {
        foreach (Ingredient ingredient in ingredientsList)
        {
            if (!ingredient.GetIsSliced()) 
            {
                Debug.Log("All Sliced");
                return false;
            }
        }
        return true;
    }

    // The Ingredients architecture is terrible, I need to fix that
    public void ResetIngredientsList() 
    {
        allSliced = false;
        ingredientsList = null;
    }

    public List<Ingredient> GetIngredientsList() 
    {
        if (ingredientsList == null) 
        {
            ingredientsList = new List<Ingredient>();
            List<string> ingredientsListString = Kitchen.BreakFoodIndex(_foodIndex);
            foreach (string ingredientString in ingredientsListString) 
            {
                Ingredient newIngredient = new Ingredient(ingredientString);
                /* WARNING: This is not the best way to do it */
                if (ingredientString == "rice") 
                {   
                    // Slicing the rice, you always have rice in the kitchen
                    newIngredient.SetSliced();
                }

                ingredientsList.Add(newIngredient);
            }
        }
        return ingredientsList;
    }

    //Check if the food is the same as the "Compare Food"
    public bool IsTheSameFood(Food compareFood) 
    {
        if (_foodIndex == compareFood.GetFoodIndex()) 
        {
            return true;
        }
        else 
        {
            return false;
        }
    }

    public void PlaySlicedAudio() 
    {
        if (_audioSource != null)
        {
            Debug.Log("AFTER SOUND");
            Debug.Log(_audioSource.clip);
            _audioSource.Play();
        }
    }

    public virtual void SliceFood() 
    {
        PlaySlicedAudio();
        if (_prefabSliced != null) 
        {
            float angleMultiplier = 180 / _slicedParts ;
            for (int i = 0; i < _slicedParts; i++)
            {
                bool topIngredient = false;
                float angle = angleMultiplier;
                if (i % 2 == 0) 
                {
                    angle = -angleMultiplier;
                    topIngredient = true;
                } 
                //spread rotation
                Quaternion spreadRotation = Quaternion.Euler(-angle, angle, 0f);

                //add rotation to the current location
                Quaternion spawnRotation = transform.rotation * spreadRotation;

                //Spawn the bullet
                GameObject newIngredient = Instantiate(_prefabSliced, transform.position, spawnRotation);
                if (topIngredient) 
                {
                    newIngredient.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 100f, 0f));
                }

                Destroy(newIngredient, 0.5f);
                

            }
        }
    }



    /*public string GenerateFoodIndex() 
    {
        //return _name
    }*/

}
