﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ingredient 
{
    private string name;
    private bool isSliced;

    public Ingredient(string newName) 
    {
        name = newName;
        isSliced = false;
    }

    public string GetName()
    {
        return name;
    }

    public bool GetIsSliced() 
    {
        return isSliced;
    }

    public void SetSliced() 
    {
        isSliced = true;
    }

}
