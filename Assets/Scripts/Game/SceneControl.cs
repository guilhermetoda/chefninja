﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour {
    
    [SerializeField] private GameObject _pauseScreen;

    // load scene from string
    public void LoadScene (string sceneName)
    {
        if (Time.timeScale == 0) 
        {
            Time.timeScale = 1;
        }
        SceneManager.LoadScene(sceneName);
    }

    // quit game
    public void ExitGame()
    {
        Application.Quit();
    }

    public void ResumeGame() 
    {
        Time.timeScale = 1;
        _pauseScreen.SetActive(false);
    }
	
}