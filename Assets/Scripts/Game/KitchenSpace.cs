﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenSpace : MonoBehaviour
{
    [SerializeField] private int _kitchenCounterIndex;

    public int GetKitchenCounterIndex() 
    {
        return _kitchenCounterIndex;
    }

    // generates the spawn position to the Food in the Kitchen Counter Space
    public Vector3 GeneratesSpawnPosition(Transform kitchenCounterPosition) 
    {
        //for now, hardcoded the position, changing only the Y-Axis
        return new Vector3(0f,0.8f,0f) + kitchenCounterPosition.transform.position;
    }

    /* private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Player")) 
        {
            if (gameObject.CompareTag("KitchenCounterUsed")) 
            {
                Food food = transform.GetChild(0).GetComponent<Food>();
                if (food != null) 
                {
                    cakeslice.Outline outliner = food.GetComponent<cakeslice.Outline>();
                    if (outliner != null) 
                    {
                        if (outliner.enabled) 
                        {
                            outliner.enabled = false;
                        }
                    }
                }
            }
        }
    }*/
}
