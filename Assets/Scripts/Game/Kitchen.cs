﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class to control the game and the position of the kitchen

public class Kitchen : MonoBehaviour
{
    public static int _countersNumber = 14;

    public static string[] kitchenPositions;
    public static bool _isKitchenFull = false;
    
    public static List<KitchenOrder> kitchenOrders;

    public static AudioSource _audioSource;

    private void Awake() {
        _audioSource = GetComponent<AudioSource>();
        kitchenPositions = new string[_countersNumber];
        kitchenOrders = new List<KitchenOrder>();
        InitKitchen();
    }
    // Creates the empty position on the kitchen
    private void InitKitchen() {
        
        for (int i = 0; i < kitchenPositions.Length; i++) 
        {
            kitchenPositions[i] = "empty";
        }
        
    }
    // Gets a empty position on the kitchen
    public static int GetRandomKitchenPosition() 
    {
        bool foundPosition = false;
        int position = -1;
        if (!_isKitchenFull) 
        {
            while (!foundPosition) 
            {
                int randomIndex = Random.Range(0,kitchenPositions.Length);
                if (kitchenPositions[randomIndex] == "empty") 
                {
                    position = randomIndex;
                    foundPosition = true;
                }
            }
            
        }
        return position;
        
    }
    // adds a food to a position
    public static void AddFoodToKitchenPosition(int position, string foodIndex)
    {
        kitchenPositions[position] = foodIndex;
    }
    // creates a food on the counter
    public static void InstatiateFood(Transform kitchenCounterPosition, string foodIndex)
    {
        GameObject food = FoodList.foodList[foodIndex];
        Vector3 newPosition = new Vector3(0f,0.8f,0f) + kitchenCounterPosition.transform.position;
        GameObject instantiatedFood = Instantiate(food, newPosition, food.transform.rotation);
        instantiatedFood.transform.parent = kitchenCounterPosition;
    }
    // Add food to the counter
    public static void AddFoodToKitchenSpace(string foodIndex)
    {
        
        /* int kitchenPosition = GetRandomKitchenPosition();
        if (kitchenPosition >=0) 
        {
            AddFoodToKitchenPosition(kitchenPosition, foodIndex);
        }*/

        GameObject[] freeKitchenCounters = GameObject.FindGameObjectsWithTag("KitchenCounterFree");

        if (freeKitchenCounters.Length > 0) 
        {
            int randomIndex = Random.Range(0, freeKitchenCounters.Length);
            
            InstatiateFood(freeKitchenCounters[randomIndex].transform, foodIndex);
            _audioSource.Play();
            //Making the Count unable to receive food
            freeKitchenCounters[randomIndex].tag = "KitchenCounterUsed";
        }
        else 
        {
            // THere is no empty spaces
        }

        
    }

    // Combine the string with |
    public static string JoinString(string string1, string string2) 
    {
        return string1+"|"+string2;
    }

    // Breaks the string in a list
    public static List<string> BreakFoodIndex(string foodIndex) 
    {   
        string[] stringArray = foodIndex.Split('|');
        List<string> list = new List<string>(stringArray);
        return list;
    }

    // Generates a possible food index
    public static string GeneratePossibleFoodCode(List<string> foodCombination) 
    {
        foodCombination.Sort();
        return string.Join("|",foodCombination);
    }

    // Checks if the food exists on the food List (Dictionary)
    public static bool CheckFood(string foodIndex) 
    {
        if (FoodList.foodList.ContainsKey(foodIndex)) 
        {
            return true;
        }
        return false;
    }

    
    public static bool CanDeliveryMeal(Food deliveredMeal) 
    {
        if (CheckAndRemoveCorrectOrder(deliveredMeal)) 
        {
            Coins.IncrementCoins(deliveredMeal.GetMealScore());
            
            return true;
        }
        else {
            return false;
        }
    }


    public static bool CheckAndRemoveCorrectOrder(Food meal) 
    {
        for (int i = 0; i < kitchenOrders.Count; i ++ )
        {
            if (meal.IsTheSameFood(kitchenOrders[i].GetFood())) 
            {
                UIGetOrders.RemovingOrderFromCanvas(kitchenOrders[i].GetCanvasIndex());
                kitchenOrders.Remove(kitchenOrders[i]);
                return true;
            }
        }
        return false;
        
    }

    // Spawn Kitchen Orders
    public static void SpawnNinjaOrder(KitchenOrder ninjaOrder) 
    {
        bool findValidFood = false;
        Food newFood = null;
        while (!findValidFood) 
        {
            // This is the index of the list
            int mealListIndex = Random.Range(0, FoodList.mealList.Count);
            // this is the index of the meal on the food
            string mealString = FoodList.mealList[mealListIndex];
            newFood = FoodList.foodList[mealString].GetComponent<Food>();
            findValidFood = newFood.IsValidFoodForLevel();
        }

        if (newFood != null) 
        {
            
            int mealIndex = UIGetOrders.SetNewImageToCanvas(newFood.GetSprite());
            KitchenOrder newKitchenOrder = new KitchenOrder(newFood, mealIndex);
            kitchenOrders.Add(newKitchenOrder);
        }
    }

    // Spawn Kitchen Orders
    public static void SpawnOrders() 
    {

        bool findValidFood = false;
        Food newFood = null;
        while (!findValidFood) 
        {
            // This is the index of the list
            int mealListIndex = Random.Range(0, FoodList.mealList.Count);
            // this is the index of the meal on the food
            string mealString = FoodList.mealList[mealListIndex];
            newFood = FoodList.foodList[mealString].GetComponent<Food>();
            findValidFood = newFood.IsValidFoodForLevel();
        }

        if (newFood != null) 
        {
            
            int mealIndex = UIGetOrders.SetNewImageToCanvas(newFood.GetSpriteNinjaScene());
            print(mealIndex);
            KitchenOrder newKitchenOrder = new KitchenOrder(newFood, mealIndex);
            //kitchenOrders.Add(newKitchenOrder);
            NinjaScreenOrders.ninjaOrders.Add(newKitchenOrder);
        }
    }

}
