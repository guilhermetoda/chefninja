﻿using System.Collections;
using System.Collections.Generic;

public class KitchenOrder 
{
    private Food _food;
    private int _canvasIndex;

    public KitchenOrder(Food newFood, int canvasIndex) 
    {
        SetFood(newFood);
        SetCanvasIndex(canvasIndex);
    }

    public Food GetFood() 
    {
        return _food;
    }

    public int GetCanvasIndex() 
    {
        return _canvasIndex;
    }

    public void SetCanvasIndex(int newCanvasIndex) 
    {
        _canvasIndex = newCanvasIndex;
    }

    public void SetFood(Food newFood) 
    {
        _food = newFood;
    }
}
