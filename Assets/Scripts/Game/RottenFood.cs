﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RottenFood : Food
{
    // This Rotten Food will make the player slow if he collids with the food or if he tries to slice
    [Header("POISONED FOOD! Effects")]
    [SerializeField] private float _slowTimeInSeconds = 3f;
    [SerializeField] private bool _jumpBlocked = false;
    [SerializeField] private bool _attackBlocked = false;
    //[SerializeField] private Color _colorOutlineEffect;

    public override void OnCollisionEffect(Collision other) 
    {
        // other is a player
        HorizontalMovement player = other.gameObject.GetComponent<HorizontalMovement>();
        player.SetSlowEffect(_slowTimeInSeconds, _jumpBlocked, _attackBlocked);
        InstantiateEffectsAndSound();
    }

    private void InstantiateEffectsAndSound() 
    {
        PlaySlicedAudio();
        if (_prefabSliced != null) 
        { 
            gameObject.GetComponent<cakeslice.Outline>().enabled =false;
            gameObject.GetComponent<Renderer>().enabled = false;
            Destroy(gameObject.transform.GetChild(0).transform.gameObject);
            GameObject newIngredient = Instantiate(_prefabSliced, transform.position, Quaternion.identity);
            Destroy(newIngredient, 3f);
        }

    }

    public void SlicePoisonFood(HorizontalMovement player) 
    {
        player.SetSlowEffect(_slowTimeInSeconds, _jumpBlocked, _attackBlocked);
        InstantiateEffectsAndSound();
    }

}
