﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProgress : MonoBehaviour
{

    [SerializeField] private int[] _levelCoinsRequired;
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _audioClipWin;
    [SerializeField] private AudioClip _audioClipLose;
    [SerializeField] private int _levelStart = 0;
    [SerializeField] private GameObject _prefabMinus10;

    private static int _wasteFoodLosingPoints = 10;
    private static int _thrashFoodLosingPoints = 5;

    private static Dictionary<string, int> _foodPoints;
    private static int _thrashPointsLost = 0;
    private static int _wasteFoodPointsLost = 0;

    public static bool tutorial = false;

    private static int _coinsRequired = 0;
    private static int _levelProgress = 0;
    private static bool _playerPassedLevel = false;
    private static int _maxLevel = 2;

    private void Awake()
    {
        //_levelProgress = _levelStart;
        //Debug.Log("Current level BIF: "+_levelProgress);
        if (_playerPassedLevel) {
            _levelProgress++;
        }
        //Debug.Log("Current level AIF: "+_levelProgress);
        _foodPoints = new Dictionary<string, int>();
        _thrashPointsLost = 0;
        _wasteFoodPointsLost = 0;
        _coinsRequired = _levelCoinsRequired[_levelProgress];
        _playerPassedLevel = false;
    }

    public static void ResetGame() 
    {
        _levelProgress = 0;
        tutorial = false;
        _playerPassedLevel = false;
    }

    public static void SetLevel(int newLevel) 
    {
        _levelProgress = newLevel;
    }

    public static void SetCoinsRequired(int newCoinsRequired)
    {
        _coinsRequired = newCoinsRequired;
    }

    public static bool IsGameFinished() 
    {
        if (_levelProgress == _maxLevel) 
        {
            return true;
        }
        return false;
    }

    public static int GetCoinsRequired() 
    {
        return _coinsRequired;
    }

    public static Dictionary<string, int> GetFoodPoints() 
    {
        return _foodPoints;
    }

    public static void IncreaseThrashPoints() 
    {
        _thrashPointsLost +=_thrashFoodLosingPoints;
    }

    public static void WasteFoodPoints() 
    {
        _wasteFoodPointsLost += _wasteFoodLosingPoints;
    }

    public static void IncreasingDeliveryFoodPoints(string foodIndex, int points) 
    {
        if (_foodPoints.ContainsKey(foodIndex)) 
        {
            _foodPoints[foodIndex]+= points;
        }
        else 
        {
            _foodPoints[foodIndex] = points;
        }
    }

    public static void DebugDictionary() 
    {
        foreach(var item in _foodPoints)
        {       
            Debug.Log(item.Key+" : "+item.Value);
            
        }
    }

    public static bool GetIfPlayerPassedLevel() 
    {
        return _playerPassedLevel;
    }

    private static int GetNegativePoints() 
    {
        GameObject[] kitchenUsedSpaces = GameObject.FindGameObjectsWithTag("KitchenCounterUsed");
        
        return -kitchenUsedSpaces.Length*_wasteFoodLosingPoints;
    }

    public static void EndLevel() 
    {
        Coins.IncrementCoins(GetNegativePoints());
       
        if (CheckIsLevelHasPassed(Coins.GetCoins())) 
        {
            _playerPassedLevel = true;
            //_levelProgress++;
        }
        else 
        {
            _playerPassedLevel = false;
        }
    }

    public static bool CheckIsLevelHasPassed(int coinsEarned) 
    {
        if (_coinsRequired <= coinsEarned) 
        {
            return true;
        }

        return false;
    }

    public static int GetLevel() 
    {
        return _levelProgress;
    }

    public void PlaySound() 
    {
        if (_playerPassedLevel) 
        {
            _audioSource.clip = _audioClipWin;
        }
        else 
        {
            _audioSource.clip = _audioClipLose;
        }
        _audioSource.Play();
    }

}
