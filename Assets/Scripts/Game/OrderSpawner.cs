﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderSpawner : MonoBehaviour
{
    [SerializeField] private float _spawnCooldown = 10f;
    [SerializeField] private int _maxOrders = 6;

    private float _spawnCounter = 8f;
    private static bool _stopSpawning = false;

    // Spawn one Meal at every _spawnCooldown seconds
    private void Update()
    {

        if (_spawnCounter >= _spawnCooldown && !_stopSpawning && _maxOrders > (NinjaScreenOrders.ninjaOrders.Count + Kitchen.kitchenOrders.Count))
        {
            Kitchen.SpawnOrders();
            _spawnCounter = 0;
        }
        else
        {
            _spawnCounter += Time.deltaTime;
        }
    }

    public void StopSpawning()
    {
        _stopSpawning = true;
    }

}
