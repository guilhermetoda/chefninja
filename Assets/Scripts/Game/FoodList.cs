﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodList : MonoBehaviour
{
    public static Dictionary<string, GameObject> foodList;

    public static List<string> mealList; 

    // At the beginning of the game, all the possible foods and prefabs are loaded to the memory inside the foodList Dictionary
    private void Awake() 
    {
        // Dictionary with all the foods and prefabs
        foodList = new Dictionary<string, GameObject>();    
        // List with all the food that can be ordered
        mealList = new List<string>();
        GameObject[] foodPrefabList = Resources.LoadAll<GameObject>("FoodPrefab");
        for (int i=0; i < foodPrefabList.Length; i++) 
        {
            if (foodPrefabList[i] != null) 
            {
                Food foodComponent = foodPrefabList[i].GetComponent<Food>();
                foodList.Add(foodComponent.GetFoodIndex(), foodPrefabList[i]);
                // Check if the Food is Meal
                if (foodComponent.GetIsMeal())
                {
                    // Add the meal to the "Meal List" to spawn meals as Order in the Kitchen
                    mealList.Add(foodComponent.GetFoodIndex());
                }

            }
            
        }

    }
}
