﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaScreenOrders : MonoBehaviour
{
    public static List<KitchenOrder> ninjaOrders;

    private void Awake()
    {
        ninjaOrders = new List<KitchenOrder>();
    }

    public static void CheckSlicedIngredient(string foodIndex) 
    {
        Debug.Log(ninjaOrders.Count);
        foreach (KitchenOrder order in ninjaOrders) 
        {
           
            Food food = order.GetFood();

            // not a good thing, calling this every time because of the first time i need to initialize that
            food.GetIngredientsList();
            Debug.Log(foodIndex+" : "+food.GetFoodIndex());
            if (food.SliceIngredient(foodIndex)) 
            {
                Debug.Log("HEREE SLICED");
                if (food.GetAllSliced()) 
                {
                    Kitchen.kitchenOrders.Add(order);
                    ninjaOrders.Remove(order);
                    food.ResetIngredientsList();
                    UIGetOrders.SetImageToKitchenScreen(food.GetSprite(), order.GetCanvasIndex());
                    UIGetOrders.RemovingOrderFromNinjaScreen(order.GetCanvasIndex());
                    // All Sliced
                    // Remove from ninja Order
                    // Add to Kitchen order

                    
                }
                break;
            }
        }
    }

    public static void PrintIngredientList(List<string> newListOfPossibleIngredients) 
    {
        print("NEW LIST");
        foreach (string index in newListOfPossibleIngredients) 
        {
            print(index);
        }
    }

    public static List<string> CreateListForSpawnIngredients() 
    {
        List<string> newListOfPossibleIngredients = new List<string>();
        newListOfPossibleIngredients.Add("rottentomato");
        foreach (KitchenOrder order in ninjaOrders) 
        {
            Food food = order.GetFood();
            print(food.GetFoodIndex());
            foreach (Ingredient ingredient in food.GetIngredientsList()) 
            {
                if (!ingredient.GetIsSliced()) 
                {
                    // Double the chance of spawn food than tomato
                    newListOfPossibleIngredients.Add(ingredient.GetName());
                    newListOfPossibleIngredients.Add(ingredient.GetName());
                }
            }
        }
        return newListOfPossibleIngredients;
    }

}
