﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class to control ingredients in the ninja screen
public class IngredientsList : MonoBehaviour
{
    [SerializeField] private float _spawnCooldown = 3f; // the time between spawning items
    [SerializeField] private bool _isCreditsScene = false;

    public static Dictionary<string, GameObject> ingredients;
    public static List<GameObject> ingredientList;

    private float currentCooldown = 0;

    private static bool stopSpawning = false;

    private void Awake() 
    {
        // Gets all prefabs from the directory (this will be easy to the art creates more ingredients)
        IngredientsList.ingredients = new Dictionary<string, GameObject>();    
        ingredientList = new List<GameObject>();
        GameObject[] ingredientsPrefabList = Resources.LoadAll<GameObject>("IngredientPrefab");
        for (int i=0; i < ingredientsPrefabList.Length; i++) 
        {
            if (ingredientsPrefabList[i] != null) 
            {
                Food foodComponent = ingredientsPrefabList[i].GetComponent<Food>();
                IngredientsList.ingredients.Add(foodComponent.GetFoodIndex(),ingredientsPrefabList[i]);
                if (foodComponent.GetFoodIndex() != "rottentomato") 
                {
                    ingredientList.Add(ingredientsPrefabList[i]);
                }
                
            }
            
        }
    }

    // Spawn one ingredient at every _spawnCooldown seconds
    private void Update()
    {
        if (currentCooldown >= _spawnCooldown && !stopSpawning)
        {
            // Credits Scene
            if (_isCreditsScene) 
            {
                CreateNewIngredientUsingAllPrefabs();
            }
            else 
            {
                CreateNewIngredient();
            }
            currentCooldown = 0;
        }
        else
        {
            currentCooldown += Time.deltaTime;
        }
    }

    // Spawn a random ingredient on a random position (up, middle or bottom)
    private void CreateNewIngredientUsingAllPrefabs() 
    {
        //(20, possibleY, -32)
        //possible Y = 7,8,9
        float[] possibleYPosition = new float[3];
        possibleYPosition[0] = 7f;
        possibleYPosition[1] = 8f;
        possibleYPosition[2] = 9f;

        int randomIndex = Random.Range(0, possibleYPosition.Length);

        Vector3 spawnPosition = new Vector3(transform.position.x, possibleYPosition[randomIndex], -32);
        Quaternion spreadRotation = Quaternion.Euler(0f, 90f, 0f);
        int randomIngredientIndex = 0;
        randomIngredientIndex = Random.Range(0, ingredientList.Count);
        Instantiate(ingredientList[randomIngredientIndex],spawnPosition, spreadRotation);
       
        
    }

    // Spawn a random ingredient on a random position (up, middle or bottom)
    private void CreateNewIngredient() 
    {
        //(20, possibleY, -32)
        //possible Y = 7,8,9
        float[] possibleYPosition = new float[3];
        possibleYPosition[0] = 7f;
        possibleYPosition[1] = 8f;
        possibleYPosition[2] = 9f;

        int randomIndex = Random.Range(0, possibleYPosition.Length);

        Vector3 spawnPosition = new Vector3(transform.position.x, possibleYPosition[randomIndex], -32);
        Quaternion spreadRotation = Quaternion.Euler(0f, 90f, 0f);
        bool findValidIngredient = false;
        int randomIngredientIndex = 0;
        List<string> possibleListOfIngredients = NinjaScreenOrders.CreateListForSpawnIngredients();
        NinjaScreenOrders.PrintIngredientList(possibleListOfIngredients);
        string ingredientIndexName = "";
        while (!findValidIngredient) 
        {
            randomIngredientIndex = Random.Range(0, possibleListOfIngredients.Count);
            ingredientIndexName = possibleListOfIngredients[randomIngredientIndex];
            findValidIngredient = ingredients[ingredientIndexName].GetComponent<Food>().IsValidFoodForLevel();
        }
        if (ingredientIndexName != "") 
        {
            Instantiate(ingredients[ingredientIndexName],spawnPosition, spreadRotation);
        }
        
    }

    public void StopSpawning()
    {
        stopSpawning = true;
    }
}
