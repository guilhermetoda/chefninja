﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] protected float _velocity = 10f; // foward launch velocity
    [SerializeField] private float _lifeTime = 5f; // life time until destruction

    protected Rigidbody _rigidBody; //rigidbody on the projectile

    private void Awake()
    {
        // get the rigidbody and apply velocity
        _rigidBody = GetComponent<Rigidbody>();
        _rigidBody.velocity = transform.forward * _velocity;

        //destroy after life time ends
        Destroy(gameObject, _lifeTime);
    }

    private void OnCollisionEnter(Collision other)
    {
        //Destroy(gameObject);    
        if (other.gameObject.CompareTag("Player")) 
        {               
            RottenFood rottenFood = GetComponent<RottenFood>();
            if (rottenFood != null) 
            {
                rottenFood.OnCollisionEffect(other);
                
                return;
            }
        }
        // IF Object hits player
        
    }

}