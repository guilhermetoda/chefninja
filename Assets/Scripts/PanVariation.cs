﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanVariation : MonoBehaviour
{
    private AudioSource _audio;
    private bool isAdding = false;

    private void Awake()
    {
        _audio = GetComponent<AudioSource>();    
    }

    private void Update()
    {
        if (_audio.panStereo >= 1f) 
        {
            _audio.panStereo -= Time.deltaTime;     
            isAdding = false;
        }
        else if (_audio.panStereo <= -1f) 
        {
            _audio.panStereo += Time.deltaTime; 
            isAdding = true;
        }
        
        if (isAdding) 
        {
            _audio.panStereo += Time.deltaTime /5f;
        }
        else 
        {
            _audio.panStereo -= Time.deltaTime /5f;
        }
        
    }
}
