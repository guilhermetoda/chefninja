%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: NinjaMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: master_controller
    m_Weight: 0
  - m_Path: master_controller/l_leg_controller
    m_Weight: 0
  - m_Path: master_controller/r_leg_controller
    m_Weight: 0
  - m_Path: master_controller/root_controller
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL/shoulder_controller
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL/shoulder_controller/hat_controller
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL/shoulder_controller/l_bow_controller
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL/shoulder_controller/L_Shldr_CNTRL
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL/shoulder_controller/L_Shldr_CNTRL/l_elbow_controller
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL/shoulder_controller/L_Shldr_CNTRL/l_elbow_controller/l_arm_controller
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL/shoulder_controller/r_bow_controller
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL/shoulder_controller/R_Shldr_CNTRL
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL/shoulder_controller/R_Shldr_CNTRL/r_elbow_controller
    m_Weight: 0
  - m_Path: master_controller/root_controller/LowerBack_CNTRL/shoulder_controller/R_Shldr_CNTRL/r_elbow_controller/r_arm_controller
    m_Weight: 0
  - m_Path: polySurface16
    m_Weight: 0
  - m_Path: root
    m_Weight: 1
  - m_Path: root/l_upper_leg
    m_Weight: 1
  - m_Path: root/l_upper_leg/l_knee
    m_Weight: 1
  - m_Path: root/l_upper_leg/l_knee/l_ankle
    m_Weight: 1
  - m_Path: root/l_upper_leg/l_knee/l_ankle/l_foot
    m_Weight: 1
  - m_Path: root/l_upper_leg/l_knee/l_ankle/l_foot/l_toe
    m_Weight: 1
  - m_Path: root/low_back
    m_Weight: 1
  - m_Path: root/low_back/upper_body
    m_Weight: 1
  - m_Path: root/low_back/upper_body/bow
    m_Weight: 1
  - m_Path: root/low_back/upper_body/bow/l_fabric_node
    m_Weight: 1
  - m_Path: root/low_back/upper_body/bow/l_fabric_node/l_fabric_middle
    m_Weight: 1
  - m_Path: root/low_back/upper_body/bow/l_fabric_node/l_fabric_middle/l_fabric_tip
    m_Weight: 1
  - m_Path: root/low_back/upper_body/bow/r_fabric_node
    m_Weight: 1
  - m_Path: root/low_back/upper_body/bow/r_fabric_node/r_fabric_middle
    m_Weight: 1
  - m_Path: root/low_back/upper_body/bow/r_fabric_node/r_fabric_middle/r_fabric_tip
    m_Weight: 1
  - m_Path: root/low_back/upper_body/head_tip
    m_Weight: 1
  - m_Path: root/low_back/upper_body/head_tip/hat_tip
    m_Weight: 1
  - m_Path: root/low_back/upper_body/l_scapula
    m_Weight: 1
  - m_Path: root/low_back/upper_body/l_scapula/l_shoulder
    m_Weight: 1
  - m_Path: root/low_back/upper_body/l_scapula/l_shoulder/l_elbow
    m_Weight: 1
  - m_Path: root/low_back/upper_body/l_scapula/l_shoulder/l_elbow/l_wrist
    m_Weight: 1
  - m_Path: root/low_back/upper_body/l_scapula/l_shoulder/l_elbow/l_wrist/l_fingers
    m_Weight: 1
  - m_Path: root/low_back/upper_body/l_scapula/l_shoulder/l_elbow/l_wrist/l_fingers/joint1
    m_Weight: 1
  - m_Path: root/low_back/upper_body/r_scapula
    m_Weight: 1
  - m_Path: root/low_back/upper_body/r_scapula/r_shoulder
    m_Weight: 1
  - m_Path: root/low_back/upper_body/r_scapula/r_shoulder/r_elbow
    m_Weight: 1
  - m_Path: root/low_back/upper_body/r_scapula/r_shoulder/r_elbow/r_wrist
    m_Weight: 1
  - m_Path: root/low_back/upper_body/r_scapula/r_shoulder/r_elbow/r_wrist/r_fingers
    m_Weight: 1
  - m_Path: root/r_upper_leg
    m_Weight: 1
  - m_Path: root/r_upper_leg/r_knee
    m_Weight: 1
  - m_Path: root/r_upper_leg/r_knee/r_ankle
    m_Weight: 1
  - m_Path: root/r_upper_leg/r_knee/r_ankle/r_foot
    m_Weight: 1
  - m_Path: root/r_upper_leg/r_knee/r_ankle/r_foot/r_toe
    m_Weight: 1
